module.exports = function (app) {
		
	app.root.addCommand('help', function(req, res) {
		var commandRoute = req.parameters._ || [],
			command = app.root.getCommandForRoute(commandRoute);

		if(!command.getParent()) {
			res.caption('Usage info for wybe\'s humble fonto tool');
			res.log('It\'s meant to make a developer\'s life easier in times of error-prone, frequent, repetitive or mundane tasks.');
		} else {
			res.caption('Usage info for "' + command.name + '"');
			res.properties({
				'Route': command.getRoute().join(' '),
				'Description': command.description
			});
		}

		if(command.listCommands().length) {
			res.caption('Child commands');
			res.properties(command.listCommands().sort(function(a, b) {
				return a.name < b.name ? -1 : 1;
			}).reduce(function (commands, cmd) {
				commands[cmd.name] = cmd.description;
				return commands;
			}, {}));
		}

		if(command.parameters.length) {
			res.caption('Parameters');
			res.properties(command.parameters.reduce(function (parameters, param) {
				parameters[param.name] = param.description;
				return parameters;
			}, {}));
		}

		if(command.options.length) {
			res.caption('Options');
			res.properties(command.options.sort(function(a, b) {
				return a.long < b.long ? -1 : 1;
			}).reduce(function (options, option) {
				var formattedKey = (option.short ? '-' + option.short : '  ') + '    ' + '--' + option.long,
					formattedDescription = (option.required ? '* ' : '') + option.description;
				options[formattedKey] = formattedDescription;
				return options;
			}, {}));
		}

	}).isGreedy()
};
