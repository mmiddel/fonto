require('harmonize')();

var path = require('path'),
	fs = require('fs'),
	glob = require('glob');

// awesomesauce by michel & martin

var FAMILIES_NAMESPACE_PREFIX = 'fontoxml-families/';


var p = Proxy.createFunction({
	get: function (obj, value) {return value === 'toString' ? function () {return '';} : p;}
}, function () {
	return p;
});


module.exports = function (app) {
	function listFamilyUsageCommand (req, res) {
		var dump = function () { return console.log.apply(this, arguments); };

		var console = {
			log: function () {},
			warn: function () {}
		};

		var accumulatedInfo = [];

		// Mock define method
		var define = function(namespaces, callback) {
			var doesUseFamilies = false;

			var callbackArgs = [];
			namespaces.forEach(function(ns) {
				if (ns.indexOf(FAMILIES_NAMESPACE_PREFIX) === 0) {
					doesUseFamilies = true;
					callbackArgs.push(function onFamilyRegister (familyName, _, elementName, hrName) {
						accumulatedInfo.push([typeof elementName === 'string' ? elementName : '[Selector]', '"' + hrName + '" (' + familyName + ')']);
						return p;
					}.bind(undefined, ns.replace(FAMILIES_NAMESPACE_PREFIX + 'configureAs', '')));
					return;
				}

				callbackArgs.push(p);
			});

			if (!doesUseFamilies) {
				res.debug('Skipped: Package fontoxml-families is not used in this file.');
				return;
			}

			callback.apply(
				Object.create(callback.prototype),
				callbackArgs
			)(p);
		};

		glob.sync(
			path.join(app.getWorkingDirectory(), '**', 'configureSxModule.js'),
			null
		).forEach(function(file){
			res.caption('CVK families used in ' + file + '...');
			try {
				eval(fs.readFileSync(file, 'utf8') || '');
			} catch (e) {
				res.error('Could not eval configureSxModule.js: ' + e.message);
			}
			res.properties(accumulatedInfo);
			accumulatedInfo = [];
		});;
	}

	var operCommand = app.root.addCommand('families', listFamilyUsageCommand)
		.addDescription('Nerf!');
};