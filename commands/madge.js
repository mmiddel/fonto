var path = require('path'),
	fs = require('fs'),
	Promise = require('promise'),
	madge = require('madge');

function getDirNamesWithinDir (dir) {
	return fs.readdirSync(dir).filter(function(fileName) {
		return fs.statSync(path.join(dir, fileName)).isDirectory();
	});
}
function arrayUnique (arr){
	var n = {}, r=[];
	for(var i = 0; i < arr.length; i++) {
		if (n[arr[i]])
				continue;
		n[arr[i]] = true; 
		r.push(arr[i]); 
	}
	return r;
}

function listAmdDependencies (packageLocation) {
	var dependencyObject = madge(packageLocation, {
		format: 'amd',
		findNestedDependencies: true
	});

	return arrayUnique(Object.keys(dependencyObject.tree)
		.reduce(function (allDependencies, fileName) {
			return allDependencies.concat(dependencyObject.tree[fileName]);
		}, [])
		.map(function (dependency) {
			return dependency.split('/')[0];
		}))
		.filter(function (dependency) {
			return dependency !== 'src';
		});
}

var specialAmdDependencies = [
	'editor',
	'configureSxModules'
];

module.exports = function (app) {

	function checkForPackage (packageName, packageRoot, req, res) {
		var packageLocation = path.join(app.getWorkingDirectory(), packageRoot, packageName),
			amdDependencies = listAmdDependencies(packageLocation);

		try {
			var bowerJson = require(path.join(packageLocation, 'bower.json'));
		} catch (e) {
			return res.notice(packageName + ' bower.json could not be read: ' + e.message);
		}

		var brokenDependencies = amdDependencies.filter(function (dependency) {
			return specialAmdDependencies.indexOf(dependency) === -1 && (!bowerJson.dependencies || !bowerJson.dependencies[dependency]);
		});

		if(brokenDependencies.length)
			res.log(packageName + ' has ' + brokenDependencies.length + ' dependencies not described in bower.json');
		else
			res.debug(packageName + ' is OK');

		amdDependencies.forEach(function (dependency) {
			if (!bowerJson.dependencies || !bowerJson.dependencies[dependency]) {
				if(specialAmdDependencies.indexOf(dependency) >= 0) {
					return res.debug('\t- ' + dependency + ' is special and is not a dependency through bower');
				} else {
					return res.log('\t- ' + dependency + ' is missing');
				}
			}

			if (
				dependency.indexOf('fontoxml-') === 0 && (
				bowerJson.dependencies[dependency] !== 'git@bitbucket.org:liones/' + dependency + '.git#develop'
				&& bowerJson.dependencies[dependency] !== '../' + dependency
				)
			) {
				res.debug('\t- ' + dependency + ' has deviant git pointer:');
				res.debug('\t  ' + bowerJson.dependencies[dependency]);
			}
		});

		if(req.options['obsolete-amds'])
			Object.keys(bowerJson.dependencies).filter(function (dependencyName) {
				return amdDependencies.indexOf(dependencyName) === -1;
			}).forEach(function (dependencyName) {
				return res.debug('\t- ' + dependencyName + ' is not used as AMD module');
			});
	}

	function checkObsoleteForPackage (packageName, packageRoot, req, res) {
		var packageLocation = path.join(app.getWorkingDirectory(), packageRoot, packageName),
			amdDependencies = listAmdDependencies(packageLocation);

		try {
			var bowerJson = require(path.join(packageLocation, 'bower.json'));
		} catch (e) {
			res.notice(e.message);
			return;
		}

		var brokenDependencies = !bowerJson.dependencies ? [] : Object.keys(bowerJson.dependencies).filter(function (dependency) {
			return specialAmdDependencies.indexOf(dependency) === -1 && amdDependencies.indexOf(dependency) === -1;
		});

		if (!brokenDependencies.length) {
			res.debug(packageName + ' happens to use all it\'s bower dependencies in Javascript');
			return;
		}

		res.notice(packageName + ' has ' + brokenDependencies.length + ' dependencies that are not in AMD');

		brokenDependencies.forEach(function (dependency) {
			return res.log('\t- ' + dependency);
		});
	}

	function madgeCommand (req, res) {
		var packageRoot = typeof req.options.lib === 'string'
			? req.options.lib :
			(req.options.lib
				? 'lib'
				: path.join('src', 'packages')
			);

		var checkObsolete = req.options.obsolete;

		try {
			if(req.parameters.package) {
				res.caption('Comparing AMD dependencies with bower.json for package "' + req.parameters.package + '"');
				return checkObsolete
					? checkObsoleteForPackage(req.parameters.package, packageRoot, req, res)
					: checkForPackage(req.parameters.package, packageRoot, req, res);
			} else {
				res.caption('Comparing AMD dependencies with bower.json in ' + packageRoot);
				getDirNamesWithinDir(path.join(app.getWorkingDirectory(), packageRoot)).map(function (packageName) {
					return checkObsolete
						? checkObsoleteForPackage(packageName, packageRoot, req, res)
						: checkForPackage(packageName, packageRoot, req, res);
				});
			}
		} catch(e) {
			res.error(e.stack);
			return;
		}

		if(checkObsolete)
			res.log('Listed items may be the less obvious dependencies through SCSS or Jade mixins (etc.) a package might have.');

		res.success('Comparison complete');

	}

	app.root.addCommand('madge', madgeCommand)
		.addDescription('Check the bower.json file for missing dependencies, based on the AMD define() declarations. Be wary, since it does not consider relations to SCSS or Jade')
		.addOption('obsolete', 'o', 'List dependencies that are not an AMD dependency')
		.addParameter('package', 'The application-level package to check')
		.addOption('lib', 'l', 'If set, check the lib/ dir');
};