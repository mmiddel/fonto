var Slack = require('slack-node'),
	Promise = require('promise');

var slackWebhooks = {
	'thed': 'https://hooks.slack.com/services/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',
	'liones': 'https://hooks.slack.com/services/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',
	'p0mp': 'https://hooks.slack.com/services/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'
};

module.exports = function (app) {
	function slackCommand (req, res) {
		res.caption('Posting to slack');

		if(!slackWebhooks[req.options.site])
			return res.error('The slack site "'+req.options.site+'" is not configured.');

		var slack = new Slack(),
			messageConfig = {
				channel: '#' + (req.options.channel || 'general'),
				username: req.options.name || 'FontoBot',
				text: req.options.message
			};;

		slack.setWebhook(slackWebhooks[req.options.site]);

		res.properties(messageConfig);
		res.debug('POST ' + slackWebhooks[req.options.site]);

		return new Promise(function (resolve, reject) {
			slack.webhook(messageConfig, function(err, response){
				if(err)
					return reject(err);

				if(response.status === 'ok')
					res.success('Posted!');
				else
					res.error('Could not post message: error code ' + response.statusCode + ' (' + response.status + ')');
				resolve();
			});
		});
	}

	app.root.addCommand('slack', slackCommand)
		.addOption('channel', 'c', 'The channel to post to (defaults to "general")')
		.addOption('name', 'n', 'The name of the bot to use (defaults to "FontoBot")')
		.addOption('site', 's', 'The slack site to post to', true)
		.addOption('message', 'm', 'An actual message to post', true);
};