var path = require('path'),
	glob = require('glob'),

	arrayUnique = require('../lib/util/arrayUnique');

// Code by Martin "the Man in the" Middel
// Notice: there's crap from Wybe here as well

// Array.prototype.find not available in my version of node, so here goes
function arrayFind(arr, fn) {
	for(var i = 0; i < arr.length; ++i) {
		if(fn.call(arr, arr[i], i, arr))
			return arr[i];
	}
}

module.exports = function (app) {
	function hasContentModelForNodeName (nodeName, contentModel) {
		return contentModel.items
			? contentModel.items.some(hasContentModelForNodeName.bind(undefined, nodeName))
			: (contentModel.localName === nodeName);
	}

	function getContentModelRefFor (schema, nodeDef) {
		return schema.contentModels.reduce(function (accum, cm, i) {
			return hasContentModelForNodeName(nodeDef, cm)
				? accum.concat([i])
				: accum;
		}, []);
	}

	function getElementsReferringCM (schema, ref) {
		return schema.elements.reduce(function (accum, element) {
			return element.contentModelRef === ref
				? accum.concat([element])
				: accum;
		}, []);
	}

	function getNodeNamesReferringContentModel (schema, nodeDef) {
		return getContentModelRefFor(schema, nodeDef).reduce(function (accum, ref) {
			return accum.concat(getElementsReferringCM(schema, ref));
		}, []);
	}

	function flattenContentModel (contentModel, cM) {
		if(!cM) cM = [];

		if(contentModel.items)
			return contentModel.items.reduce(function (accum, item) {
				return accum.concat(flattenContentModel(item))
			}, cM);

		cM.push(contentModel);
		return cM;
	}

	function elementCommand (req, res) {
		var nodeName = req.parameters.element,
			schemaFileName = path.join(app.getWorkingDirectory(), req.options.schema);

		res.caption('Summary of element <' + nodeName + '>');
		res.debug('Reading from ' + schemaFileName);

		var schemaFile = require(schemaFileName),
			nodeDefinition = arrayFind(schemaFile.elements, function (nodeDef) {
				return nodeDef.localName === nodeName
			});

		if(!nodeDefinition)
			return res.error('The element <'+nodeName+'> is not described in this schema.json file');

		res.properties({
			'localName': nodeDefinition.localName,
			'Documentation': nodeDefinition.documentation,
			'Mixed content': nodeDefinition.isMixed ? 'Yes' : 'No',
			'Abstract': nodeDefinition.isAbstract ? 'Yes' : 'No'
		});

		res.caption('Could be contained by...');
		res.log(getNodeNamesReferringContentModel(schemaFile, nodeName)
			.map(function (element) {
				return element.localName;
			}).sort().join(' / ') || 'This element has no parents...?');

		res.caption('Could contain...');
		res.log(arrayUnique(
			flattenContentModel(schemaFile.contentModels[nodeDefinition.contentModelRef])
				.map(function (element) {
					return element.localName;
				})
		).sort().join(' / ') || 'This element has no children...');

		res.caption('Atributes');
		res.log(arrayUnique(
			nodeDefinition.attributeRefs.map(function (attributeRef) {
					return schemaFile.attributes[attributeRef].localName;
				})
		).sort().join(' / ') || 'This element has no attributes...');
	}

	function attributeCommand (req, res) {
		var attrName = req.parameters.attribute,
			schemaFileName = path.join(app.getWorkingDirectory(), req.options.schema);

		res.caption('Summary of attribute @' + attrName);
		res.debug('Reading from ' + schemaFileName);

		var schemaFile = require(schemaFileName),
			attrDefinition = arrayFind(schemaFile.attributes, function (attrDef) {
				return attrDef.localName === attrName
			}),
			attrIndex = schemaFile.attributes.indexOf(attrDefinition);

		if(!attrDefinition)
			return res.error('The attribute @'+attrName+' is not described in this schema.json file');

		res.properties({
			'localName': attrDefinition.localName,
			'Type localName': attrDefinition.typeLocalName,
			'Type namespace': attrDefinition.typeNamespace,
			'Use': attrDefinition.use,
			'Default value': attrDefinition.defaultValue
		});

		if(attrDefinition.allowedValues) {
			res.caption('Allowed values');
			res.log(attrDefinition.allowedValues.sort().join('\n'));
		}

		res.caption('Elements');
		res.log(arrayUnique(
			schemaFile.elements.filter(function (element) {
				return element.attributeRefs.indexOf(attrIndex) >= 0;
			}).map(function (element) {
				return element.localName;
			})
		).sort().join(' / ') || 'This attribute is not used on any elements...');
	}

	app.root.addCommand('element', elementCommand)
		.addParameter('element', 'Query a specific element', true)
		.addOption('schema', 's', 'Use schema file', true);

	app.root.addCommand('attribute', attributeCommand)
		.addParameter('attribute', 'Query a specific attribute', true)
		.addOption('schema', 's', 'Use schema file', true);
};