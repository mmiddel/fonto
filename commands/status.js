var timeAgo = require('../lib/util/timeAgo');

module.exports = function (app) {
	var workingDir = app.getWorkingDirectory();

	function summarizeBranches(res, branches) {
		res.properties(Object.keys(branches).reduce(function (branchesSummary, branchName) {
			var branch = branches[branchName];
			branchesSummary[branchName] = branch.committed_date + ', ' + timeAgo(branch.committed_date) + ' ago';
			return branchesSummary;
		}, {}));
	}

	app.root.addCommand('status', function (req, res) {
		res.caption('Repository status');
		return app.raiseCode(['git']).then(function (code) {
			var git = code.getAddon('git'),
				dirtyFiles = git.dirtyFiles;//.filter(function (df) { return df.tracked; });

			//if(!git.isGitControlled()) {
			//	res.error(code.path + ' is not controlled through git');
			//	return;
			//}

			res.properties({
				'Path': code.path,
				'Remote': git.originUrl,
				'Current branch': git.currentBranch,
				'Detached head': git.detachedHead
			});

			res.caption('Uncommitted files');
			if(dirtyFiles.length)
				res.properties(dirtyFiles.map(function (dirtyFile) {
					var symbol = 'Untracked';
					if(dirtyFile.type)
						if(dirtyFile.type.indexOf('A') >= 0)
							symbol = 'Added';
						else if(dirtyFile.type.indexOf('D') >= 0)
							symbol = 'Deleted';
						else if(dirtyFile.type.indexOf('R') >= 0)
							symbol = 'Moved';
						else if(dirtyFile.type.indexOf('M') >= 0)
							symbol = 'Modified';
					return [symbol, dirtyFile.file];
				}));
			else
				res.log('No uncommitted files');

			res.caption('Local branches');
			summarizeBranches(res, git.branches);

			res.caption('Remote branches');
			summarizeBranches(res, git.remotes);

			//res.caption('AMD Dependencies');
			//var deps = code.getAddon('dependencies').dependencies;
			//res.properties(Object.keys(deps).map(function (dep) {
			//	dep = deps[dep];
			//	var status = '';
			//	if(!dep.amd) status = 'Not in AMD';
			//	if(!dep.bower) status = 'Not in bower';
			//	return [
			//		status,
			//		dep.name
			//	];
			//}));
		});
	});
};
