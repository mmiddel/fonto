var path = require('path'),
	fs = require('fs'),
	del = require('del'),
	lnk = require('lnk'),
	Promise = require('promise'),
	mergeDirs = require('merge-dirs'),
	bower = require('bower');

var GitRepo = require('../lib/GitRepo');

function getDirectoriesInPath(path) {
	return fs.readdirSync(path).filter(function (file) {
		return fs.lstatSync(path+'/'+file).isDirectory() || fs.lstatSync(path+'/'+file).isSymbolicLink();
	});
}

module.exports = function (app) {
	var workingDir = app.getWorkingDirectory();

	function getRepoLocationForPackage (packageName) {
		return path.join(workingDir, '..', '..', 'libs', packageName);
	}

	function getTemporaryLocationForPackage (packageName) {
		return getRepoLocationForPackage(packageName) + '.temporary';
	}

	function getLibLocationForPackage (packageName) {
		return path.join(workingDir, 'lib', packageName);
	}

	function getPointerForPackage (packageName) {
		return 'git@bitbucket.org:liones/' + packageName + '.git';
	}

	/**
	 * Make sure the mentioned package is no longer symlinked, but instead installed by bower
	 * @param req
	 * @returns {Promise}
	 */
	function blinkOffCommand (req, res) {
		var packageName = req.parameters.package,
			libDirectoryName = getLibLocationForPackage(packageName);

		// Do not unblink a package that is not symlinked, it would be madness
		if(!fs.lstatSync(libDirectoryName).isSymbolicLink())
			throw new Error('Package "' + packageName + '" is not currently linked in this application');

		res.caption('Unblinking "' + packageName + '"');

		res.log('Removing symlink to package "' + packageName + '"');
		del.sync([libDirectoryName]);

		// Wrap "bower install" in a promise

		// Installing *all* missing packages is better than installing just one because one package may
		// depend on another, or even have nested dependencies that cannot be easily resolved outside of Bower.
		res.log('Installing all missing packages through Bower');
		return new Promise(function (resolve, rej) {
			bower.commands.install([], {}, {
				verbose: true
			})
			.on('log', function (message) {
				res.debug(message.id + '\t' + message.message);
			})
			.on('prompt', function (prompt, callback) {
				res.notice('Bower prompted "'+prompt+'"');
				app.term.prompt(prompt).then(function (response) {
					res.debug('Prompt reply: ' + response);
					callback(response)
				});
			})
			.on('end', function () {
				resolve(true);
			})
			.on('error', function (err) {
				rej(err);
			})
		}).then(function () {
				res.success('Unblinked package "' + packageName + '"');
			return true;
		});
	}



	function blinkOn(res, packageName, branchName, keepLocalChanges, updateExistingGit) {
		if(!packageName)
			throw new Error('You must specify a package name');

		var remoteName = getPointerForPackage(packageName),
			directoryName = getRepoLocationForPackage(packageName),
			temporaryDir = getTemporaryLocationForPackage(packageName),
			libDirectoryName = getLibLocationForPackage(packageName),
			gitRepo = new GitRepo(remoteName, directoryName),
			promiseChain = null,
			lstat = null;

		res.caption('Blinking package "' + packageName + '"');
		res.properties({
			'Remote': remoteName,
			'Branch': branchName,
			'Directory': directoryName,
			'Keeping local changes': keepLocalChanges,
			'Update existing git': updateExistingGit
		});

		// Do not blink a package that is already symlinked, it would be madness
		try {
			lstat = fs.lstatSync(libDirectoryName);
		} catch(e) {
			res.debug('Could not lstat "' + packageName + '": '+ e.message);
		}

		if(lstat && lstat.isSymbolicLink()) {
			res.error('Package "' + packageName + '" is already linked in this application');
			if(updateExistingGit)
				res.notice('You can run the "update" command instead');
			return;
		}

		// If the git repo folder actually exists...
		if(gitRepo.exists()) {

			res[updateExistingGit ? 'log' : 'notice']('Package "' + packageName + '" is already cloned in the expected location.');
			// ... and user wants to update, promise to check out the latest version
			if(updateExistingGit) {
				res.log('Checking out the latest version of branch "' + branchName + '"');
				promiseChain = gitRepo.checkoutLatest(branchName);

				// ... and user specifies a branch, do not fetch but do checkout
			} else if(branchName) {
				res.log('Checking out local branch "'+branchName+'"');
				promiseChain = gitRepo.checkout(branchName);

				// ... or if not updating, resolve immediately
			} else {
				res.log('Not touching existing local repository');
				promiseChain = Promise.resolve(true);
			}


			// @TODO:
			// if $package_name already exists and commit hash is different
			//     warn or (if not $force) ask confirmation
			//     set incongruency flag

			// If the git repo folder does not exist
		} else {
			res.log('Cloning package "' + packageName + '"');

			// Promise to clone it, and check out the given branch name
			promiseChain = gitRepo.clone()
				.then(function () {
					res.log('Checking out branch "' + branchName + '"');
					return gitRepo.checkout(branchName);
				})
				.then(function () {
					// And then brag about it
					res.log('Cloning "' + packageName + '" from ' + remoteName + ' into ' + directoryName + ' done.');
				});
		}

		// If keeping local changes, make a temporary copy of bower's files
		// @NOTICE: has been known to silently fail when moving to somewhere outside CWD
		if (keepLocalChanges) {
			promiseChain = promiseChain.then(function () {
				res.log('Keeping local changes, temporarily moving lib/ files to ' + temporaryDir + '');
				fs.renameSync(libDirectoryName, temporaryDir);
				return true;
			});

		// If not keeping local changes, throw away files in the lib/ location
		} else {
			promiseChain = promiseChain.then(function () {
				res.log('Not keeping local changes, removing "' + packageName + '" from lib/ directory');
				del.sync([path.join(libDirectoryName)]);
			});
		}

		// If and when we get to this point, actually make the symbolic link from the git location to fontoxml's lib dir
		promiseChain = promiseChain.then(function () {
			res.log('Symlinking cloned repository to lib/ directory');
			lnk.sync([directoryName], path.join(workingDir, 'lib'), {
				type: 'directory'
			});
		});

		// If keeping local changes
		if (keepLocalChanges) {
			promiseChain = promiseChain
				// copy the files from the bower install into the git repo
				.then(function () {
					res.log('Restoring local changes');
					mergeDirs(temporaryDir, directoryName, true); // somehow this lib does it synchronously, which I don't mind
				})
				// ...and remove the temporary copy
				.then(function () {
					res.log('Cleaning up temporary copy of local changes');
					del.sync([temporaryDir], {
						force: true // Is outside of CWD, so needs to be forced
					});
				});
		}

		return promiseChain.then(function () {
			res.success('Package "' + packageName + " is now blinked.");
			return true;
		});

	}

	/**
	 * Replace a bower lib with a symbolic link to a git repo.
	 * @param req
	 * @returns {*}
	 */
	function blinkOnCommand (req, res) {
		var packageName = req.parameters.package,
			branchName = req.options.branch || 'develop',
			keepLocalChanges = !!req.options.keep,
			updateExistingGit = !!req.options.update;
		return blinkOn(res, packageName, branchName, keepLocalChanges, updateExistingGit);
	}

	// "list" - list information about blinked packages
	function blinkListCommand (req, res) {
		var listUnlinked = req.options.unlinked,
			libPath = path.join(app.getWorkingDirectory(), 'lib'),
			blinkedPackages = getDirectoriesInPath(libPath).filter(function (packageName) {
				console.log(packageName + ': ' + fs.lstatSync(path.join(libPath, packageName)).isSymbolicLink())
				// Pick out directories that are actually symbolic links
				return packageName.indexOf('fontoxml-') === 0
					&& fs.lstatSync(path.join(libPath, packageName)).isSymbolicLink() === !listUnlinked;
			});

		// Early return if no packages are blinked
		if (blinkedPackages.length < 1) {
			res.success('Done, no packages currently ' + (listUnlinked ? 'un' : '') + 'linked.');
			return;
		}

		res.success('Found ' + blinkedPackages.length + ' ' + (listUnlinked ? 'un' : '') + 'linked packages');

		// Get the git status for all blinked packages in parallel
		return Promise.all(blinkedPackages.map(function (packageName) {
			if(listUnlinked) {
				res.log('    - ' + packageName);
				return;
			}
			var repo = new GitRepo(
				getPointerForPackage(packageName),
				getRepoLocationForPackage(packageName)
			);

			// git status && log
			return repo.status().then(function(status) {
				res.log('    - ' + packageName + (status.clean ? '' : ' (dirty)'));
			});
		})).then(function () {
			return true;
		});
	}



	// "list" - list information about blinked packages
	function blinkRelinkCommand (req, res) {
		var libPath = path.join(app.getWorkingDirectory(), 'lib'),
			linkPath = path.join(app.getWorkingDirectory(), '..', '..', 'libs'),
			checkedoutPackages = getDirectoriesInPath(linkPath),
			blinkedPackages = getDirectoriesInPath(libPath).filter(function (packageName) {
				// Pick out directories that are actually symbolic links
				return packageName.indexOf('fontoxml-') === 0
					&& checkedoutPackages.indexOf(packageName) >= 0;
			});

		res.caption('Relinking available package repositories');

		if (blinkedPackages.length < 1) {
			res.success('No packages currently relinkable.');
			return;
		}

		// Get the git status for all blinked packages in parallel
		return Promise.resolve(blinkedPackages.map(function (packageName) {
			res.log('Linking up ' + packageName + ' from ' + getRepoLocationForPackage(packageName));
			del.sync([getLibLocationForPackage(packageName)]);
			lnk.sync([getRepoLocationForPackage(packageName)], path.join(workingDir, 'lib'), {
				type: 'directory'
			});
		})).then(function () {
			res.success(blinkedPackages.length + ' packages relinked');
			blinkedPackages.forEach(function (packageName) {
				res.log('    - ' + packageName);
			});
			return true;
		});
	}

	function listPackages (workingDir, listUnlinked) {
		var libPath = path.join(app.getWorkingDirectory(), 'lib');

		return getDirectoriesInPath(libPath).filter(function (packageName) {
			// Pick out directories that are actually symbolic links
			return fs.lstatSync(path.join(libPath, packageName)).isSymbolicLink() === !listUnlinked;
		});
	}

	// "list" - list information about blinked packages
	function blinkUpdateCommand (req, res) {
		var listUnlinked = req.options.unlinked,
			blinkedPackages = listPackages(path.join(app.getWorkingDirectory(), 'lib'), listUnlinked);

		res.caption('Updating ' + (listUnlinked ? 'un' : '') + 'linked packages.');
		// Early return if no packages are blinked
		if (blinkedPackages.length < 1) {
			res.success('Done, no packages currently ' + (listUnlinked ? 'un' : '') + 'linked.');
			return;
		}

		res.debug('Updating ' + blinkedPackages.length + ' ' + (listUnlinked ? 'un' : '') + 'linked packages from lib/ location');

		if(listUnlinked) {
			del.sync(blinkedPackages.map(getLibLocationForPackage));

			return new Promise(function (resolve, rej) {
				bower.commands.install([], {}, {
					verbose: true
				})
				.on('log', function (message) {
					res.debug(message.id + '\t' + message.message);
				})
				.on('prompt', function (prompt, callback) {
					res.log('Bower prompted "'+prompt+'"');
					app.term.prompt(prompt).then(function (response) {
						res.debug('Prompt reply: ' + response);
						callback(response)
					});
					res.debug(message);
				})
				.on('end', function () {
						resolve(true);
				})
				.on('error', function (err) {
					rej(err);
				})
			}).then(function () {
				res.log('Reinstalled packages through bower');
				return true;
			});
		} else {
			// Get the git status for all blinked packages in parallel
			return Promise.all(blinkedPackages.map(function (packageName) {
				var repo = new GitRepo(
					getPointerForPackage(packageName),
					getRepoLocationForPackage(packageName)
				);
			})).then(function () {
				res.log('Updated ' + blinkedPackages.length + ' packages through git');
				return true;
			});
		}
	}

	/*
		Register controller functions to a command tree
	 */
	var blinkCommand = app.root.addCommand('blink')
		.addDescription('Link or unlink FontoXML dependencies');

	blinkCommand.addCommand('list', blinkListCommand)
		.addOption('unlinked', 'u', 'List only unlinked packages');

	blinkCommand.addCommand('relink', blinkRelinkCommand);

	blinkCommand.addCommand('update', blinkUpdateCommand)
		.addOption('unlinked', 'u', 'Update only unlinked packages');

	blinkCommand.addCommand('on', blinkOnCommand)
		.addParameter('package', 'Package name to be blinked')
		.addOption('keep', 'k', 'Keep local changes in the lib/ location')
		.addOption('branch', 'b', 'Specify a branch, defaults to "develop"')
		.addOption('update', 'u', 'Update any existing files along the way');

	blinkCommand.addCommand('off', blinkOffCommand)
		.addParameter('package', 'Package name to be unblinked')
		.addOption('clean', 'c', 'Also remove the files that were linked to');
};
