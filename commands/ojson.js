var path = require('path'),
	glob = require('glob');


module.exports = function (app) {
	// List usages of unique operation steps
	function operCheckUniqueStepUsageCommand (req, res) {
		res.caption('Checking operations for step types');
		return app.raiseCode(['operations']).then(function (code) {
			var operations = code.getAddon('operations'),
				steps = operations.groupOperationsByStep(req.options.type, req.options.name);

			Object.keys(steps).forEach(function (stepType) {
				var typeResults = steps[stepType];
				Object.keys(typeResults).forEach(function (stepId) {
					res.caption(stepType + '/' + stepId);
					res.properties(typeResults[stepId].map(function (operation) {
						return [operation.operationName, operation.operationFile];
						//res.debug('\t' + operation.operationName + '\t\t' + operation.operationFile);
					}));
				});
			});
		});
	}

	var operCommand = app.root.addCommand('ojson')
		.addDescription('Some informative commands concerning operations.json');

	operCommand.addCommand('steps', operCheckUniqueStepUsageCommand)
		.addDescription('List usages of unique operation steps')
		.addOption('type', 't', 'Summarize these type of steps')
		.addOption('name', 'n', 'Summarize only step name');
};