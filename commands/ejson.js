var path = require('path'),
	fs = require('fs'),
	glob = require('glob');

module.exports = function (app) {

	// Re-orders the content of any and all element.jsons alphabetically by localName
	function ejsonSortCommand (req, res) {
		var workingDirectory = app.getWorkingDirectory(),
			elementsJsons = glob.sync('**/elements.json', {
				cwd: workingDirectory
			});

		res.caption('Reordering all elements.json files in '+workingDirectory);

		elementsJsons.forEach(function (elementsJson) {
			var filePath = path.join(workingDirectory, elementsJson);

			// See if we can actually read the JSON
			try {
				var elements = require(filePath);
			} catch(e) {
				res.error(elementsJson + ' is not valid JSON, skipping');
				return;
			}

			// Sort the entries in elements.json by their localName
			var elementsOrdered = elements.sort(function (a, b) {
				return a.localName === b.localName
					? 0
					: (a.localName < b.localName ? -1 : 1);
			});

			// Write the reordered elements.json back to filesystem
			fs.writeFileSync(filePath, JSON.stringify(elementsOrdered, null, '\t'));

			res.log('Rewriting to ' + filePath);
		});

		res.success('Rewrote ' + elementsJsons.length + ' element.json files');
	}

	// Gives an overview of all declared human-readable names per element, and the location of where it was configured
	function ejsonIndexCommand (req, res) {
		var lookForConflicts = req.options.conflics,
			workingDirectory = app.getWorkingDirectory(),
			elementsJsons = glob.sync('**/elements.json', {
				cwd: workingDirectory
			});

		var longestElementName = '';

		var elementConfigs = elementsJsons
			// Throw all element information from across the elements.jsons onto one big pile
			.reduce(function (config, elementsJson) {
				return config.concat(
					require(path.join(workingDirectory, elementsJson))
						.map(function (elementConfig) {
							elementConfig.elementsJson = elementsJson;
							return elementConfig;
						})
				);
			},[])

			// Sort the pile by localName
			.sort(function (a, b) {
				return a.localName < b.localName ? -1 : (a.localName === b.localName ? 0 : 1);
			})

			// Save an array of configs to their element name (an element may have multiple elements.jsons)
			.reduce(function (configByName, elementConfig) {
				if(elementConfig.localName.length > longestElementName.length)
					longestElementName = elementConfig.localName;

				configByName[elementConfig.localName] = configByName[elementConfig.localName]
					? configByName[elementConfig.localName].concat([elementConfig])
					: [elementConfig];
				return configByName;
			}, {});

		// Dump to console
		res.caption('Listing human readable names for elements, per element, per element.json');
		Object.keys(elementConfigs).forEach(function (elementName) {
			var elementConfig = elementConfigs[elementName];
			var hasConflict = (lookForConflicts && !elementConfig.every(function (c) {
				return c.label === elementConfig[0].label;
			}));
			elementConfig.forEach(function (c, i) {
				res[(hasConflict || !lookForConflicts) ? 'log' : 'debug'](
					(i === 0
						? elementName + (new Array(longestElementName.length - elementName.length + 1)).join(' ')
						: (new Array(longestElementName.length + 1)).join(' '))
					+ '\t' + c.label + '\t\t' + c.elementsJson);
			});
		});

	}

	var ejsonCommand = app.root.addCommand('ejson')
		.addDescription('Various commands concerning elements.json. Does not consider CVK stuff.');

	ejsonCommand.addCommand('index', ejsonIndexCommand)
		.addDescription('List all human readable names for every element described in an elements.json, and the location')
		.addOption('conflics', 'c', 'Highlight only conflicting human-readable names');

	ejsonCommand.addCommand('sort', ejsonSortCommand)
		.addDescription('Reorder any elements.json alphabetically on localName');
};