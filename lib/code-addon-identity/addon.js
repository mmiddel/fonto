var path = require('path'),

	Promise = require('promise');

function IdentityAddon (code) {
	this.name = null;
	this.type = null;
	this.fonto = false;

	this.refresh(code);
}

function safeReadJson (path) {
	try {
		return require(path);
	} catch(e) {
		return false;
	}
}

// @TODO: put this somewhere configurable
function shouldBeConfigurableCodeToNameResolver(code) {
	return path.parse(code.path).name;
}

IdentityAddon.prototype.refresh = function (code, gitAddon) {
	this.bower = safeReadJson(path.join(code.path, 'bower.json'));
	this.npm = safeReadJson(path.join(code.path, 'package.json'));

	// From a list of possible sources, take the first option that is not falsy
	// and use the path lib to parse the basename from it
	this.name = path.parse([
		gitAddon ? gitAddon.originUrl : null,
		(this.bower || {}).name,
		(this.npm || {}).name,
		shouldBeConfigurableCodeToNameResolver(code)
	].filter(function (v) { return !!v; })[0]).name;

	this.fonto = this.name.indexOf('fontoxml-') === 0;

	this.type = this.name.indexOf('-app-') !== -1
		? 'app'
		: 'package';

	return Promise.resolve();
};

IdentityAddon.prototype.init = function (code, gitAddon) {
	this.refresh(code, gitAddon);

	return Promise.resolve();
};

IdentityAddon.prototype.hasNamingInconsistency = function () {
	return (!this.bower || this.name === this.bower.name) && (!this.npm || this.name === this.npm.name);
};

module.exports = ['git', IdentityAddon];