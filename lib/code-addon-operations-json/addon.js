var path = require('path'),
	glob = require('glob'),
	Promise = require('promise');

function OperationsJsonAddon (code) {
	this.operations = [];
}

function safeReadJson (path) {
	try {
		return require(path);
	} catch(e) {
		return false;
	}
}

OperationsJsonAddon.prototype.refresh = function (code) {
	var workingDirectory = code.path;

	return new Promise(function (resolve, reject) {
		glob('**/operations.json', {
			cwd: workingDirectory
		}, function (err, operationsJsons) {
			if(err)
				return reject(err);

			// Break the list of operation.json files down into a long array of operation definition objects
			this.operations = operationsJsons.reduce(function (allOps, operationsJson) {
				var operations = require(path.join(workingDirectory, operationsJson));
				return allOps.concat(Object.keys(operations)
						.filter(function (operationName) {
							return !!operations[operationName];
						})
						.map(function (operationName) {
							var operation = operations[operationName];
							operation.operationName = operationName;
							operation.operationFile = operationsJson;
							return operation;
						})
				);
			}, []);

			return resolve();
		}.bind(this));
	}.bind(this));

};


OperationsJsonAddon.prototype.groupOperationsByStep = function (stepTypes, stepIds) {
	stepTypes = !stepTypes ? null : (Array.isArray(stepTypes) ? stepTypes : [stepTypes]);
	stepIds = !stepIds ? null : (Array.isArray(stepIds) ? stepIds : [stepIds]);

	return this.operations.reduce(function (results, operation) {
		(Array.isArray(operation.steps) ? operation.steps : [operation.steps]).forEach(function (step) {
			if(!step)
				return;

			var stepType = step.type.substr(0, step.type.indexOf('/')),
				stepId = step.type.substr(step.type.indexOf('/') + 1);

			if(stepTypes && stepTypes.indexOf(stepType) === -1) return;
			if(stepIds && stepIds.indexOf(stepId) === -1) return;

			if(!results[stepType])
				results[stepType] = {};

			Array.isArray(results[stepType][stepId])
				? results[stepType][stepId].push(operation)
				: results[stepType][stepId] = [operation];
		});
		return results;
	}, {});
};
OperationsJsonAddon.prototype.init = function (code) {
	return this.refresh(code);
};


module.exports = [OperationsJsonAddon];