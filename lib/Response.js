var os = require('os'),

	chalk = require('chalk'),
	Promise = require('promise');

function indent(string, indentation) {
	if(indentation === undefined)
		indentation = '    ';
	return indentation + string.split('\n').join(os.EOL + indentation);
}
function rpad ( str, length, char ) {
	return str + (new Array(Math.max((length || 0) - (str.length || 0) + 1, 0))).join(char || " ");
}
function Response (colors) {
	this.indentation = '    ';
}

Response.prototype._log = function (data, foregroundColor, indentation) {
	console.log(indent(chalk[foregroundColor](data), indentation));
};

/**
 * A description of proceedings relevant to the task/whatever
 * @param data
 */
Response.prototype.log = function (data) {
	return this._log(data, 'reset', this.indentation);
};

/**
 * Indicates that something the user wanted happened.
 * @param data
 */
Response.prototype.success = function (data) {
	return this._log(data, 'bold', this.indentation);
};

/**
 * Indicates the app is working on a concern/task/whatever.
 * @param data
 */
Response.prototype.caption = function (data) {
	console.log('');
	return this._log(data, 'underline');//,this.indentation);
};

/**
 * Something that is probably of interest, if not important, for the user; exceptions, search results, urgent stuff
 * @param data
 */
Response.prototype.notice = function (data) {
	return this._log(data, 'white', this.indentation);
};

/**
 * Something messed up
 * @param data
 */
Response.prototype.error = function (data) {
	return this._log(data, 'red', this.indentation);
};

/**
 * Information that the user might not even care about at that time
 * @param data
 */
Response.prototype.debug = function (data) {
	return this._log(data, 'dim', this.indentation);
};

Response.prototype.property = function (key, value, keySize) {
	console.log(indent(chalk.dim(rpad(key, keySize)) + '    ' + chalk.reset(value), this.indentation));
};

Response.prototype.properties = function (obj) {
	var maxLength = 0;
	if(Array.isArray(obj)) {
		obj.forEach(function (k) {
			maxLength = Math.max((k[0] || '').length, maxLength);
		});
		obj.forEach(function (k) {
			this.property(k[0], k[1], maxLength);
		}.bind(this));
	} else {
		Object.keys(obj).forEach(function (k) {
			maxLength = Math.max(k.length, maxLength);
		});
		Object.keys(obj).forEach(function (k) {
			this.property(k, obj[k], maxLength);
		}.bind(this));
	}
};
module.exports = Response;