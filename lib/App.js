var AskNicely = require('ask-nicely'),
	SpeakSoftly = require('./Response'),

	CodeRegistry = require('./code/CodeRegistry'),

	// Terminal = require('./Terminal'),

	Promise = require('promise');


/**
 * Yes.
 * @constructor
 */
function App (cwd) {
	this._cwd = cwd || process.cwd();

	if(!this._cwd)
		throw new Error('No working directory, app cannot run (for now)');

	this.logger = new SpeakSoftly();
	this.root = new AskNicely();
	this.code = new CodeRegistry();

}

App.prototype.getWorkingDirectory = function () {
	return this._cwd;
};
App.prototype.init = function (modules, args) {
	modules.forEach(function (module) {
		module(this);
	}.bind(this));
	return this;
};

App.prototype.interpret = function (args) {
	return this.root.interpret(args);
};
App.prototype.raiseCodeForPath = function (p, addons) {
	return CodeRegistry.codeForPath(p).init(
		this.code.getAddonDependencySubset(addons)
			.reduce(function(depObj, depName) {
				depObj[depName] = this.code.addons[depName];
				return depObj;
			}.bind(this), {})
		);
};

App.prototype.raiseCode = function (addons) {
	return this.raiseCodeForPath(this.getWorkingDirectory(), addons);
};

module.exports = App;