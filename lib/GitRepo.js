var path        = require('path'),
	fs          = require('graceful-fs'),
	Promise     = require('promise'),
	git         = require('gift'); // https://github.com/notatestuser/gift

	function recognizePointersFromString (pointerString) {
		if(!pointerString || pointerString.indexOf('.git') === -1)
			return false;

		pointerString = pointerString.split('#');

		return {
			remote: pointerString[0],
			head: pointerString[1]
		};
	}

	// @TODO: Make arguments: repoUrl or version pointer, dir
	function GitRepo(pointerString, dir) {
		this.dir = dir;

		this._repo = git(dir);
		this._busy = false;

		var pointers = recognizePointersFromString(pointerString);
		this.name = pointers.remote; // @TODO: implement or deprecate
		this.remote = pointers.remote;
		this.head = pointers.head;

	}

	/**
	 *
	 * @returns {Promise}
	 */
	GitRepo.prototype.refresh = function() {
			return Promise.all([
				this.getRemotes(),
				this.getCommits(),
				this.getCurrentCommit()
			]);
	};

	/**
	 * Check if package exists on filesystem
	 */
	GitRepo.prototype.exists = function (dir) {
		return !!fs.existsSync(dir || this.dir);
	};
	/**
	 * Clones if not exists. Fetches if it exists
	 */
	GitRepo.prototype.download = function () {
		if (this.exists()) {
			return this.fetch();
		} else {
			return this.clone();
		}
	};

	GitRepo.prototype.clone = function (pointerString) {
		if(this.exists())
			return Promise.reject(new Error('Repository "' + this.name + '" cannot be cloned because it already exists, update instead.'));

		return new Promise(function (res, rej) {
			this._busy = true;
			git.clone(
				this.remote,
				this.dir,
				function (err, result) {
					this._busy = false;

					if (err)
						return rej(err);

					this._repo = result;

					this.refresh();

					res(result);
				}.bind(this)
			)
		}.bind(this))
	};


	/**
	 * Gets a list of branches
	 */
	GitRepo.prototype.getBranches = function () {
		var self = this;

		return new Promise(function (res, rej) {
			self._repo.branches(
				function (err, result) {
					if (err)
						return rej(err);
					self.remotes = result;
					res(result);
				}
			)
		});
	};

	/**
	 * Wipes any file in the location and clones a few repo
	 */
	GitRepo.prototype.getRemotes = function () {
		var self = this;

		return new Promise(function (res, rej) {
			self._repo.remotes(
				function (err, result) {
					if (err)
						return rej(err);
					self.remotes = result;
					res(result);
				}
			)
		});
	};

	/**
	 * Wipes any file in the location and clones a few repo
	 */
	GitRepo.prototype.getCommits = function (treeish, limit, skip) {

		var self = this;

		if(!treeish)
			treeish = 'HEAD';

		return new Promise(function (res, rej) {
			if(!self.exists())
				return rej(new Error('Could not get commit info for "' + self.name + '" because repository does not exist on disk.'));
			self._repo.commits(treeish, limit, skip,
				function (err, result) {
					if (err)
						return rej(err);
					self.commits = result;
					res(result);
				}
			)
		});
	};


	/**
	 * Wipes any file in the location and clones a few repo
	 */
	GitRepo.prototype.fetch = function (remote) {
		var self = this;

		if (!remote)
			remote = 'origin';

		if(!self.exists())
			return Promise.reject(new Error('GitRepo "' + self.name + '" needs to exist on disk before fetch'));

		return new Promise(function (res, rej) {
			self._busy = true;
			self._repo.remote_fetch(remote, function (err, result) {
				self._busy = false;
				if (err)
					rej(err);
				else
				// @TODO: data is undefined, maybe return self._repo?
					res(result);
			});
		}).then(function() {
			return self.getRemotes();
		});
	};

	/**
	 * Checks out a branch, tag or commit
	 */
	GitRepo.prototype.checkout = function (treeish) {
		var self = this;

		if(!treeish)
			treeish = 'origin';

		if(!self.exists())
			return Promise.reject(new Error('GitRepo "' + self.name + '" needs to exist on disk before checkout'));

		return new Promise(function (res, rej) {
			self._busy = true;
			self._repo.checkout(treeish, function (err, result) {
				self._busy = false;

				self.refresh();

				if (err)
					rej(err);
				else
					res(result);
			});
		});
	};

	/**
	 * Checks out a branch, tag or commit
	 */
	GitRepo.prototype.status = function () {
		var self = this;

		if (!self.exists())
			return Promise.reject(new Error('GitRepo "' + self.name + '" needs to exist on disk before status'));

		return new Promise(function (res, rej) {
			self._repo.status(function (err, result) {
				if (err)
					rej(err);
				else
					res(result);
			});
		});
	};

	GitRepo.prototype.getCurrentCommit = function () {
		var self = this;

		if(!self.exists())
			return Promise.reject(new Error('GitRepo "' + self.name + '" needs to exist on disk before getting current commit'));

		return new Promise(function (res, rej) {
			self._repo.current_commit(function (err, result) {
				if (err)
					return rej(err);
				self.message = result.message;
				self.hash = result.id;
				self.date = result.committed_date;
				res(result);
			});
		});
	};

	/**
	 *
	 */
	GitRepo.prototype.checkoutLatest = function (treeish) {
		var self = this;
		return this.fetch().then(function() {
			return self.checkout(treeish);
		});
	};

module.exports = GitRepo;