var EventEmitter = require('events').EventEmitter,
	os = require('os'),
	Promise = require('promise'),
	readline = require('readline');

function indent(string, indentation) {
	if(indentation === undefined)
		indentation = '    ';
	return indentation + string.split('\n').join(os.EOL + indentation);
}

function Terminal (root, argumentsToPassOnEachExecute) {
	EventEmitter.call(this);

	// Prompt user for text input
	function prompt () {
		return new Promise(function(resolve, reject) {
				this.emit('prompt');
				repl.question('FXML > ', function (input) {
					if(!input || !(input + '').trim())
						return resolve();

					this.emit('response', input);

					try {
						var request = root.interpret(input);
						return resolve(request.execute.apply(request, argumentsToPassOnEachExecute));
					} catch(e) {
						return reject(e);
					}
				}.bind(this));
			}.bind(this))
			// Happy times, nothing broke. Re-prompt.
			.then(function() {
				this.emit('ready');
			}.bind(this))
			// On any error, dump and reprompt
			.catch(function (error) {
				// Prefer to echo the error stack back to user for this app
				console.log(indent(error.stack || error.message || error));
				this.emit('error', error);
			}.bind(this));
	}

	// Start listening to STDIN using readline
	var repl = readline.createInterface({
		input: process.stdin,
		output: process.stdout
	});

	// When application closes, echo a goodbye and exit
	repl.on('close', function () {
		console.log('');
		this.emit('close');
		process.exit();
	}.bind(this));

	this.on('error', prompt.bind(this));
	this.on('ready', prompt.bind(this));

	this.prompt = prompt;
}

// Terminal inherits from AskNicely
Terminal.prototype = Object.create(EventEmitter.prototype);
Terminal.prototype.constructor = Terminal;

module.exports = Terminal;