var path = require('path'),
	fs = require('fs'),
	Promise = require('promise'),
	madge = require('madge'),

	arrayUnique = require('../util/arrayUnique'),
	listDirectories = require('../util/listDirectories');


var specialAmdDependencies = [
	'editor',
	'configureSxModules'
];

function OperationsJsonAddon (code) {

}

function markDependenciesWithLabel (existingDependencies, label, dependencyNames) {
	return dependencyNames.reduce(function (dependencies, depName) {
		if(!dependencies[depName])
			dependencies[depName] = {
				name: depName
			};
		dependencies[depName][label] = true;
		return dependencies;
	}, existingDependencies || {});
}

OperationsJsonAddon.prototype.refresh = function (code, identity) {
	var dependencyObject = madge(path.join(code.path), {
		format: 'amd',
		findNestedDependencies: true
	});

	this.dependencies = markDependenciesWithLabel(
		this.dependencies,
		'bower',
		identity.bower
			? Object.keys(identity.bower.dependencies || {}).concat(Object.keys(identity.bower.devDependencies || {}))
			: []
	);
	//this.npmDependencies = identity.npm
	//	? Object.keys(identity.npm.dependencies || {}).concat(Object.keys(identity.npm.devDependencies || {}))
	//	: [];

	this.dependencies = markDependenciesWithLabel(
		this.dependencies,
		'amd',
		arrayUnique(Object.keys(dependencyObject.tree)
		.reduce(function (allDependencies, fileName) {
			return allDependencies.concat(dependencyObject.tree[fileName]);
		}, [])
		.map(function (dependency) {
			return  dependency.split('/')[0];
		})
		.filter(function (dependency) {
			return dependency !== 'src'
				&& dependency !== identity.name
				&& specialAmdDependencies.indexOf(dependency) === -1;
		}))
	);
};

OperationsJsonAddon.prototype.getUndocumentedDependencies = function () {
	return Object.keys(this.dependencies)
		.map(function (dep) {
			return this.dependencies[dep];
		}.bind(this))
		.filter(function (dep) {
			return !dep.bower;
		})
};

OperationsJsonAddon.prototype.init = function (code, identity) {
	console.trace('Init dependency addon');
	return this.refresh(code, identity);
};


module.exports = ['identity', OperationsJsonAddon];