var path = require('path'),
	fs = require('fs'),

	Promise = require('promise'),
	gift = require('gift'),
	originUrl = require('git-remote-origin-url');

function GitAddon () {
	this.originUrl = null;
	this.branches = {};
	this.remotes = {};
	this.currentBranch = null;
	this.detachedHead = null;
	this.dirtyFiles = [];
	this.busy = false;
	this._repo = null;
}


GitAddon.prototype.init = function (code) {
	this._repo = gift(code.path);

	if(!this.isGitControlled())
		return Promise.resolve();

	return Promise.all([
		this.updateRemotes(),
		this.updateBranches(),
		this.updateCurrentBranch(),
		this.updateStatus(),
		this.updateRemoteUrl()
	]).then(function () {
		code.emitChange();
	});
};

GitAddon.prototype.isGitControlled = function () {
	// Sometimes, node is retarded
	try {
		// what we wanna do:
		return !!fs.statSync(this._repo.dot_git);
	} catch (e) {
		return false;
	}
};

GitAddon.prototype.updateRemoteUrl = function () {
	return new Promise(function (res, rej) {
		originUrl(this._repo.path, function (err, url) {
			this.originUrl = err ? null : url;
			res();
		}.bind(this));
	}.bind(this));
};

GitAddon.prototype.updateRemotes = function () {
	return new Promise(function (res, rej) {
		this._repo.remotes(function (err, result) {
				this.remotes = err ? {} : result.reduce(function (branchInfo, branch) {
					branchInfo[branch.name] = branch.commit;
					return branchInfo;
				}, {});

				res();
			}.bind(this));
	}.bind(this));
};

GitAddon.prototype.updateBranches = function () {
	return new Promise(function (res, rej) {
		this._repo.branches(function (err, result) {
			this.branches = err ? {} : result.reduce(function (branchInfo, branch) {
				branchInfo[branch.name] = branch.commit;
				return branchInfo;
			}, {});

			res();
		}.bind(this));
	}.bind(this));
};

GitAddon.prototype.updateCurrentBranch = function () {
	return new Promise(function (res, rej) {
		this._repo.branch(function (err, result) {
			if(err) {
				this.currentBranch = null;
				this.detachedHead = true;
			} else {
				this.currentBranch = result.name;
				this.detachedHead = false;
			}

			res();
		}.bind(this));
	}.bind(this));
};

GitAddon.prototype.updateStatus = function () {
	return new Promise(function (res, rej) {
		this._repo.status(function (err, result) {
			this.dirtyFiles = err ? [] : Object.keys(result.files).map(function (file) {
				var info = result.files[file];
				info.file = file;
				return info;
			});

			res();
		}.bind(this));
	}.bind(this));
};

GitAddon.prototype.fetch = function (remote) {
	return new Promise(function (res, rej) {
		this._repo.remote_fetch(remote, function (err, result) {
			err ? rej(err) : res(result);
		});
	}.bind(this)).then(function() {
		return this.updateRemotes();
		}.bind(this));
};

/**
 * Checks out a branch, tag or commit
 */
GitAddon.prototype.checkout = function (treeish) {
	return new Promise(function (res, rej) {
		this.busy = true;
		this._repo.checkout(treeish, function (err, result) {
			this.busy = false;

			err ? rej(err) : res(result);
		}.bind(this));
	}.bind(this));
};


GitAddon.prototype.checkoutLatest = function (treeish) {
	return this.fetch().then(function() {
		return this.checkout(treeish);
	}.bind(this));
};

GitAddon.prototype.getCommitDiff = function () {

};
module.exports = [GitAddon];