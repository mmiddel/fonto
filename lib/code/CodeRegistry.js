var fs = require('fs'),
	path = require('path'),

	ObjectStore = require('object-store'),
	Promise = require('promise'),

	Code = require('./Code'),

	listDirectories = require('../util/listDirectories');


function CodeRegistry (directories) {
	this.directories = directories || [];
	this.addons = {
		'git': require('../code-addon-git/addon'),
		'identity': require('../code-addon-identity/addon'),
		'operations': require('../code-addon-operations-json/addon'),
		'dependencies': require('../code-addon-dependencies/addon')
	};
	ObjectStore.call(this, {
		requireInstanceOf: Code,
		primaryKey: 'id'
	});
}

CodeRegistry.prototype = Object.create(ObjectStore.prototype);
CodeRegistry.prototype.constructor = CodeRegistry;

CodeRegistry.prototype.addDirectory = function (directory) {
	this.directories.push(directory);
};

CodeRegistry.prototype.scan = function (filter) {
	return this.directories
		.reduce(function (codeDirectories, parentDirectory) {
			codeDirectories = codeDirectories.concat(listDirectories(parentDirectory));
			return codeDirectories;
		}, [])
		.map(function (directory) {
			return this.codeForPath (directory);
		}.bind(this))
};

CodeRegistry.prototype.init = function () {
	return Promise.all(this.scan().map(function (code) {
		// Initialize code with only the addons for this CodeRegistry
		return code.init(this.addons);
	}.bind(this)));
};

CodeRegistry.prototype.codeForPath = function (directory) {
	var code = this.get(directory);

	if(code)
		return code;

	return this.set(CodeRegistry.codeForPath(directory));
};

CodeRegistry.codeForPath = function (directory) {
	return new Code(directory);
};
CodeRegistry.prototype.getAddonDependencySubset = function (addons) {
	var addonRegistry = this.addons;

	return (function addDependenciesToList(list, addonNames) {
		if(!addonNames || !addonNames.length)
			return list;

		var newAddons = addonNames.filter(function (addonName) {
			return list.indexOf(addonName) === -1;
		});

		list = list.concat(newAddons);

		list = addDependenciesToList(list, newAddons.reduce(function (depList, depName) {
			return depList.concat(typeof addonRegistry[depName][0] === 'function' ? [] : addonRegistry[depName].filter(function (s) {
				return typeof s === 'string';
			}));
		}, []));

		return list;
	})([], addons ? (Array.isArray(addons) ? addons : [addons]) : []);
};
module.exports = CodeRegistry;