var Promise = require('promise');

function DependableObject () {
	this._addons = {};
}


DependableObject.prototype.setAddon = function (name, addon) {
	this._addons[name] = addon;
};

DependableObject.prototype.getAddon = function (name) {
	return this._addons[name];
};


DependableObject.prototype.getAddonNames = function (name) {
	return Object.keys(this._addons);
};


function bootAddons (parsee, addonsKnown, addonsDone, addonsBusy, whenDone) {
	var
		// Ignore addons that are already resolved, or resolving
		addonsWaiting = Object.keys(addonsKnown).filter(function (parserName) {
			return addonsDone.indexOf(parserName) === -1 && addonsBusy.indexOf(parserName) === -1;
		}),

		// Select parsers who have no unresolved dependencies
		addonsOpen = addonsWaiting.filter(function (parserName) {
			return addonsKnown[parserName][0].every(function (parserDependencyName) {
				return addonsDone.indexOf(parserDependencyName) >= 0;
			});
		});

	// If it seems there's nothing left to do...
	if (!addonsOpen.length) {
		// And we're not doing anything...
		if(!addonsBusy.length) {
			whenDone(new Error('There was a problem resolving all dependencies, some addons could not be initialized: ' + addonsWaiting.join(', ')));
		}
		return;// whenDone(null);
	}

	// For every addon that is ready to be initialized, start a new asynch thread
	addonsOpen.forEach(function (parserName) {
		// Instantiate and save in parent DependableObject
		var addon = new addonsKnown[parserName][1](parsee);
		parsee.setAddon(parserName, addon);

		// Mark as busy resolving
		addonsBusy.push(parserName);

		// Treat all init() functions as a promise eventually, call init() with
		// the parent DependableObject and all instantiated dependencies as arguments
		Promise.resolve(
			addon.init.apply(
				addon,
				[parsee].concat(addonsKnown[parserName][0].map(function (dependencyName) {
					return parsee.getAddon(dependencyName);
				}))
			)
		)
		.then(function () {
			// Unmark as resolving, mark as resolved
			addonsBusy.splice(addonsBusy.indexOf(parserName), 1);
			addonsDone.push(parserName);

			// If all addons are now resolved, end the asyncness. This should happen only once.
			if(addonsDone.length === Object.keys(addonsKnown).length)
				return whenDone(null);

			// If not everything is either resolved or resolving, run the whole show all over again
			if(addonsDone.length + addonsBusy.length < Object.keys(addonsKnown).length)
				bootAddons(parsee, addonsKnown, addonsDone, addonsBusy, whenDone);
		})
		.catch(function (error) {
			whenDone(error);

			// Might occur multiple times, so take out the whenDone function since we
			// shouldn't use it any more.
			whenDone = function () {};
		});
	}.bind(this));
}

DependableObject.prototype.init = function (addons, targets) {
	var bootAllTheThings = !targets || !Array.isArray(targets);

	if (!addons)
		return;

	if (!bootAllTheThings)
		var minimumAddons = targets;

	return new Promise(function (resolve, reject) {
		bootAddons(
			this,
			Object.keys(addons).reduce(function (addns, name) {
				var addonDescription = addons[name],
					addonDescription = Array.isArray(addonDescription) ? addonDescription : [addonDescription];

				var Addon = addonDescription[addonDescription.length - 1],
					dependencies = addonDescription.length > 1 ? addonDescription.slice(0, addonDescription.length - 1) : [];

				addns[name] = [dependencies, Addon];

				return addns;
			}, {}),
			Object.keys(this._addons),
			[],
			function (error) {
				if(error) reject(error);
				else resolve(this);
			}.bind(this)
		);
	}.bind(this));
};

module.exports = DependableObject;