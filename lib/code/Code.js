var path = require('path'),

	DependableObject = require('./DependableObject');


function quickHash(str) {
	var hash = 0;
	if (str.length == 0) return hash;
	for (i = 0; i < str.length; i++) {
		char = str.charCodeAt(i);
		hash = ((hash<<5)-hash)+char;
		hash = hash & hash; // Convert to 32bit integer
	}
	return hash.toString(36).replace(/\W+/g, '').toLowerCase();
}


/**
 * Describes a path of code & shit
 * @param path
 * @constructor
 */
function Code (path) {
	DependableObject.call(this);

	this._changeHandlers = [];
	this.id = quickHash(path);
	this.path = path;
}

Code.prototype = Object.create(DependableObject.prototype);
Code.prototype.constructor = Code;

Code.prototype.setAddon = function (name, addon) {
	DependableObject.prototype.setAddon.apply(this, arguments);
};
Code.prototype.onChange = function (cb) {
	this._changeHandlers.push(cb);
	return function () {
		this._changeHandlers.splice(this._changeHandlers.indexOf(cb), 1);
	}.bind(this);
};

Code.prototype.emitChange = function () {
	this._changeHandlers.forEach(function (cb) {
		cb(this);
	}.bind(this));
};

module.exports = Code;