var fs = require('fs'),
	path = require('path');

function listDirectories(srcpath) {
	try {
		return fs.readdirSync(srcpath).filter(function (file) {
			if (file.indexOf('.') === 0)
				return false;

			return fs.statSync(path.join(srcpath, file)).isDirectory();
		}).map(function (file) {
			return path.join(srcpath, file);
		});
	} catch (e) {
		return [];
	}
}

module.exports = listDirectories;