// http://stackoverflow.com/questions/26992767/limit-q-promise-concurrency-in-node-js
//
//module.exports = function promiseSequence(items, fn) {
//	return items.reduce(function(chain, item){
//		return chain.then(function(results) {
//			return fn(item).then(function(result){
//				return results.concat(result)
//			});
//		});
//	}, Q.resolve([]));
//};

var Promise = require('Promise');

module.exports = function promiseSequence(items, fn) {
	return items.reduce(function(chain, item){
		return chain.then(function(results) {
			return fn(item).then(function(result){
				return results.concat(result)
			});
		});
	}, Promise.resolve([]));
};

