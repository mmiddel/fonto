// http://stackoverflow.com/questions/3177836/how-to-format-time-since-xxx-e-g-4-minutes-ago-similar-to-stack-exchange-site
function timeAgo(ts) {
	var now = new Date(),
		delta = now.getTime() - ts.getTime();

	delta = delta/1000; //us to s

	var ps, pm, ph, pd, min, hou, sec, days;

	if(delta<=59){
		ps = (delta>1) ? "s": "";
		return delta+" second"+ps
	} else if(delta<=3599){
		min = Math.floor(delta/60);
		sec = delta-(min*60);
		pm = (min>1) ? "s": "";
		ps = (sec>1) ? "s": "";
		return Math.floor(min)+" minute"+pm+", "+Math.floor(sec)+" second"+ps;
	} else if(delta<=86399){
		hou = Math.floor(delta/3600);
		min = Math.floor((delta-(hou*3600))/60);
		ph = (hou>1) ? "s": "";
		pm = (min>1) ? "s": "";
		return Math.floor(hou)+" hour"+ph+", "+Math.floor(min)+" minute"+pm;
	} else {
		days = Math.floor(delta/86400);
		hou =  Math.floor((delta-(days*86400))/60/60);
		pd = (days>1) ? "s": "";
		ph = (hou>1) ? "s": "";
		return Math.floor(days)+" day"+pd+", "+Math.floor(hou)+" hour"+ph;
	}

}

module.exports = timeAgo;